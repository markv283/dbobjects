IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = object_id(N'[dbo].[iwd_eod_GetTotalOpenPalletsByMethod]')
          and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_GetTotalOpenPalletsByMethod]
END
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[iwd_eod_GetTotalOpenPalletsByMethod]
    @shipMethodRaw nvarchar(30),
    @badgeId nvarchar(50)
AS
BEGIN TRY

    SET NOCOUNT ON;

    DECLARE @company nvarchar(30)
    DECLARE @shipMethod nvarchar(30)
    DECLARE @loc nvarchar(10) = 'NJ'

    SET @loc =
    (
        SELECT lg.loc
        FROM [dbo].[itmlst_login] lg WITH (NOLOCK)
        WHERE lg.name = @badgeId
    )

    IF @shipMethodRaw = 'UPS'
    BEGIN
        SET @company = 'UPS'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'FedEx Ground'
    BEGIN
        SET @company = 'FEDEX'
        SET @shipMethod = 'GROUND'
    END
    ELSE IF @shipMethodRaw = 'USPS'
    BEGIN
        SET @company = 'USPS'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'Wholesale'
    BEGIN
        SET @company = 'Wholesale'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'TSC'
    BEGIN
        SET @company = 'TSC'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'FedEx Express'
    BEGIN
        SET @company = 'FEDEX'
        SET @shipMethod = 'EXPRESS'
    END
    ELSE IF @shipMethodRaw = 'PUROLATOR'
    BEGIN
        SET @company = 'PUROLATOR'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'DHL'
    BEGIN
        SET @company = 'DHL'
        SET @shipMethod = ''
    END;
    with cte
    as (SELECT osp.pallet PalletId,
               osp.day_no DayNumber,
               FORMAT(osp.date_opened, 'MM/dd/yyyy hh:mm:s tt') as Date,
               osp.login_opened LoginName,
               osp.computer_opened 'Computer',
               COUNT(ost.kid) Count,
               osp.print_label_login as LabelId
        FROM zzz_oetrack_scan_pallets osp with (NOLOCK)
            LEFT JOIN zzz_oetrack_scan ost with (NOLOCK)
                ON ost.Pallet = osp.pallet
        WHERE date_truck IS NULL
              AND Company = @company
              AND Method = @shipMethod
              AND osp.loc = @loc
        group by osp.pallet,
                 osp.day_no,
                 osp.date_opened,
                 osp.login_opened,
                 osp.computer_opened,
                 osp.print_label_login
       )
    select COUNT(*) as Count,
           @shipMethodRaw as ShipMethod
    From cte

END TRY
BEGIN CATCH

    RAISERROR(N'ERROR in SP - iwd_eod_GetTotalOpenPalletsByMethod', 16, 1);
    ROLLBACK;
END CATCH
GO


