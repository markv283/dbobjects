IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = object_id(N'[dbo].[iwd_eod_openPallet]')
          and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_openPallet]
END
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[iwd_eod_openPallet]
    @pal nvarchar(10),
    @badgeId NVARCHAR(20)
AS
BEGIN

    SET NOCOUNT ON;
    DECLARE @ErrorMessage nvarchar(4000),
            @MessageText nvarchar(max),
            @ErrorSeverity int,
            @ErrorState int;


    DECLARE @loginTrack nvarchar(20)
    select @loginTrack = login_truck
    from zzz_oetrack_scan_pallets WITH (NOLOCK)
    where kid = @pal

    DECLARE @language NVARCHAR(5);
    SELECT @language = il.lang
    FROM itmlst_login il
    WHERE il.name = @badgeId

    IF (@loginTrack IS NOT NULL)
    BEGIN
        IF @language = 'ES'
        BEGIN
            SET @MessageText = '�Este palet ha sido colocado en un cami�n!'
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1             -- state
                     );
        END
        ELSE
        BEGIN
            SET @MessageText = 'This pallet has been placed in truck!'
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1             -- state
                     );
        END
    END
    ELSE
    BEGIN
        UPDATE zzz_oetrack_scan_pallets
        SET print_label = NULL,
            print_label_login = NULL,
            Print_label_computer = NULL
        WHERE pallet = @pal

        SELECT osp.pallet as PalletId,
               osp.day_no as DayNumber,
               osp.login_opened as LoginName,
               FORMAT(osp.date_opened, 'MM/dd/yyyy hh:mm:s tt') as Date,
               osp.computer_opened 'Computer',
               COUNT(ost.kid) Count,
               osp.print_label_login as LabelId
        FROM zzz_oetrack_scan_pallets osp WITH (NOLOCK)
            LEFT JOIN zzz_oetrack_scan ost WITH (NOLOCK)
                ON ost.Pallet = osp.pallet
        WHERE osp.pallet = @pal
        group by osp.pallet,
                 osp.day_no,
                 osp.date_opened,
                 osp.login_opened,
                 osp.computer_opened,
                 osp.print_label_login
    END

END
GO


