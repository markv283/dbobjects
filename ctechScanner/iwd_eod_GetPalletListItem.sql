IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = object_id(N'[dbo].[iwd_eod_GetPalletListItem]')
          and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_GetPalletListItem]
END
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[iwd_eod_GetPalletListItem] @pal nvarchar(20)
AS
BEGIN TRY

    SET NOCOUNT ON;

    SELECT top 20
        kid as Id,
        pallet as PalletId,
        tracking as TrackingId,
        FORMAT(scan_date, 'MM/dd/yyyy hh:mm:s tt') as Date,
        *
    FROM zzz_oetrack_scan WITH (NOLOCK)
    WHERE Pallet = @pal
    ORDER BY scan_date DESC

END TRY
BEGIN CATCH

    RAISERROR(N'ERROR in SP - dbo.iwd_eod_GetPalletListItem', 16, 1);
    ROLLBACK;
END CATCH
GO


