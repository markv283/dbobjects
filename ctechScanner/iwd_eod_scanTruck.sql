IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = object_id(N'[dbo].[iwd_eod_scanTruck]')
          and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_scanTruck]
END
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[iwd_eod_scanTruck]
    @palletId nvarchar(10),
    @truck nvarchar(20),
    @deviceId nvarchar(20),
    @badgeId nvarchar(20)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @ErrorMessage nvarchar(4000),
            @MessageText nvarchar(max),
            @ErrorSeverity int,
            @ErrorState int;
    DECLARE @palCompany nvarchar(20),
            @palLocation nvarchar(20),
            @palMethod nvarchar(20),
            @truckNo nvarchar(20)
    DECLARE @language NVARCHAR(5);
    SELECT @language = il.lang
    FROM itmlst_login il
    WHERE il.name = @badgeId


    SELECT @palCompany = pl.Company,
           @palMethod = pl.Method,
           @palLocation = pl.loc,
           @truckNo = pl.truck_no
    FROM zzz_oetrack_scan_pallets pl (NOLOCK)
        LEFT JOIN zzz_oetrack_scan pal (NOLOCK)
            ON pal.pallet = pl.pallet
    WHERE pl.pallet = @palletId
    GROUP BY pl.Company,
             pl.loc,
             pl.Method,
             pl.truck_no

    IF (@palcompany != '11UPS' OR @palLocation != 'NJ')
    BEGIN

        IF (@truck <> @palCompany + @palMethod)
        BEGIN
            IF @language = 'ES'
            BEGIN
                SET @MessageText = 'Cami�n equivocado!'

                RAISERROR(   @MessageText,
                               -- Message text
                             16,
                               -- severity
                             1 -- state
                         );
            END
            ELSE
            BEGIN
                SET @MessageText = 'Wrong truck!'

                RAISERROR(   @MessageText,
                               -- Message text
                             16,
                               -- severity
                             1 -- state
                         );
            END
        END
        ELSE
        BEGIN
            UPDATE zzz_oetrack_scan_pallets
            SET truck_no = @truck,
                Company = @palcompany,
                Method = @palMethod,
                date_truck = GETDATE(),
                login_truck = @badgeId,
                computer_truck = @deviceId
            WHERE kid = @palletId

            SELECT osp.pallet PalletId,
                   osp.day_no DayNumber,
                   FORMAT(osp.date_opened, 'MM/dd/yyyy hh:mm:s tt') as Date,
                   osp.login_opened LoginName,
                   COUNT(ost.kid) Count,
                   osp.print_label_login as LabelId,
                   osp.company + osp.method as ShipMethod
            FROM zzz_oetrack_scan_pallets osp (NOLOCK)
                LEFT JOIN zzz_oetrack_scan ost (NOLOCK)
                    ON ost.Pallet = osp.pallet
            WHERE osp.pallet = @palletId
            group by osp.pallet,
                     osp.day_no,
                     osp.date_opened,
                     osp.login_opened,
                     osp.print_label_login,
                     osp.company + osp.method
            order by date_opened desc

        END
    END
END

GO


