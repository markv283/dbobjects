IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = object_id(N'[dbo].[iwd_eod_AddTrackingToPallet]')
          and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_AddTrackingToPallet]
END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[iwd_eod_AddTrackingToPallet]
    @palletId NVARCHAR(10),
    @shipMethodRaw NVARCHAR(30),
    @scannedItem NVARCHAR(300),
    @computerName NVARCHAR(20),
    @badgeId NVARCHAR(20)
AS
BEGIN TRY
    SET NOCOUNT ON;
    DECLARE @ErrorMessage NVARCHAR(4000),
            @ErrorSeverity INT,
            @ErrorState INT;
    DECLARE @company NVARCHAR(30),
            @shipMethod NVARCHAR(30),
            @loc NVARCHAR(10) = null

    DECLARE @MessageText NVARCHAR(100);
    DECLARE @language NVARCHAR(5);


    SELECT @language = il.lang, @loc  =il.loc
    FROM itmlst_login il
    WHERE il.name = @badgeId

	IF(@loc IS NULL OR @loc = '')
	BEGIN
		SET @loc = 'NJ'
	END

    IF @shipMethodRaw = 'UPS'
    BEGIN
        SET @company = 'UPS'
        SET @shipMethod = ''
    END
	ELSE IF @shipMethodRaw = 'PUROLATOR Express'
    BEGIN
        SET @company = 'PUROLATOR'
        SET @shipMethod = 'Express'
    END
    ELSE IF @shipMethodRaw = 'FedEx Ground'
    BEGIN
        SET @company = 'FEDEX'
        SET @shipMethod = 'GROUND'
    END
    ELSE IF @shipMethodRaw = 'USPS'
    BEGIN
        SET @company = 'USPS'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'Wholesale'
    BEGIN
        SET @company = 'Wholesale'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'TSC'
    BEGIN
        SET @company = 'TSC'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'FedEx Express'
    BEGIN
        SET @company = 'FEDEX'
        SET @shipMethod = 'EXPRESS'
    END
    ELSE IF @shipMethodRaw = 'PUROLATOR'
    BEGIN
        SET @company = 'PUROLATOR'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'DHL'
    BEGIN
        SET @company = 'DHL'
        SET @shipMethod = ''
    END

    DECLARE @palletIdLabelInfo NVARCHAR(20)

    SELECT @palletIdLabelInfo = print_label_login
    FROM zzz_oetrack_scan_pallets
    WHERE pallet = @palletId

    IF @palletId IS NULL
       OR @palletId = ''
    BEGIN
        IF @language = 'ES'
        BEGIN
            SET @MessageText = 'Por favor seleccione palet'
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1             -- state
                     );
        END
        ELSE
        BEGIN
            SET @MessageText = 'Please select pallet'
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1             -- state
                     );
        END
    END
    ELSE IF @palletIdLabelInfo IS NOT NULL
    BEGIN
        IF @language = 'ES'
        BEGIN
            SET @MessageText = '�La paleta ya est� cerrada!'
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1             -- state
                     );
        END
        ELSE
        BEGIN
            SET @MessageText = 'Pallet is already closed!'
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1             -- state
                     );
        END
    END
    ELSE IF @shipMethodRaw = 'TSC'
            AND LEN(@scannedItem) >= 24
    BEGIN
        SET @scannedItem = SUBSTRING(LTRIM(RTRIM(@scannedItem)), 12, 12)
    END
    ELSE IF @shipMethodRaw = 'Purolator'
            AND LEN(@scannedItem) >= 28
    BEGIN
        SET @scannedItem = SUBSTRING(LTRIM(RTRIM(@scannedItem)), 8, 16)
    END
    ELSE IF (
                LEN(RTRIM(LTRIM(@scannedItem))) = 34
                AND LEFT(@scannedItem, 3) = '420'
            )
    BEGIN
        SET @scannedItem = RIGHT(@scannedItem, 26)
    END
    ELSE IF (
                LEN(RTRIM(LTRIM(@scannedItem))) = 34
                AND LEFT(@scannedItem, 4) = '1002'
            )
    BEGIN
        SET @scannedItem = RIGHT(@scannedItem, 12)
    END
    ELSE IF (
                LEN(RTRIM(LTRIM(@scannedItem))) = 34
                AND LEFT(@scannedItem, 2) = '96'
            )
    BEGIN
        SET @scannedItem = RIGHT(@scannedItem, 12)
    END
    ELSE IF (
                LEN(RTRIM(LTRIM(@scannedItem))) = 34
                AND LEFT(@scannedItem, 4) = '1001'
            )
    BEGIN
        SET @scannedItem = RIGHT(@scannedItem, 12)
    END
    ELSE IF (
                LEN(RTRIM(LTRIM(@scannedItem))) = 34
                AND LEFT(@scannedItem, 4) = '1195'
            )
    BEGIN
        SET @scannedItem = RIGHT(@scannedItem, 12)
    END
    ELSE IF (
                LEN(RTRIM(LTRIM(@scannedItem))) = 34
                AND LEFT(@scannedItem, 4) = '1010'
            )
    BEGIN
        SET @scannedItem = RIGHT(@scannedItem, 12)
    END
    ELSE IF (
                LEN(RTRIM(LTRIM(@scannedItem))) = 22
                AND LEFT(@scannedItem, 2) = '96'
            )
    BEGIN
        SET @scannedItem = RIGHT(@scannedItem, 15)
    END
    ELSE IF (
                LEN(RTRIM(LTRIM(@scannedItem))) = 31 -- old val is 30 but len of 30 int in string = 31 chars
                OR LEN(RTRIM(LTRIM(@scannedItem))) = 34
            )
    BEGIN
        SET @scannedItem = RIGHT(@scannedItem, 22)
    END

    -- check if order invoiced 

    DECLARE @trkInfo TABLE
    (
        tracking NVARCHAR(50),
        ship_via NVARCHAR(25),
        ship_date DATETIME,
        ship_method NVARCHAR(25),
        doc__ INT,
        loc NVARCHAR(10),
        type NVARCHAR(30),
        kid INT,
        order_id NVARCHAR(20)
    )

    INSERT INTO @trkInfo
    (
        tracking,
        ship_via,
        ship_date,
        ship_method,
        doc__,
        loc,
        type,
        kid,
        order_id
    )
    SELECT trk.tracking,
           trk.ship_via,
           trk.ship_date,
           trk.ship_method,
           hed.doc__,
           hed.loc,
           'INVOICE' AS type,
           trk.kid,
           hed.order_id
    FROM oetrack trk WITH (NOLOCK)
        JOIN OE61BHED hed WITH (NOLOCK)
            ON trk.order_key = hed.order_key
    WHERE trk.tracking = @scannedItem
          AND hed.Doc_Type = 'I'
          AND trk.ship_date > GETDATE() - 180
    UNION
    SELECT trk.tracking,
           trk.ship_via,
           trk.ship_date,
           trk.ship_method,
           trn.batch AS doc__,
           trn.floc AS loc,
           'TRANSFER' AS type,
           trk.kid,
           '''order_id'
    FROM track trk WITH (NOLOCK)
        JOIN invtransfer_hed trn WITH (NOLOCK)
            ON trk.doc = trn.batch
               AND trk.ledger = 'IC'
               AND trk.Doc_Type = 'T'
    WHERE trk.tracking = @scannedItem
          AND ISNULL(trn.posted, 0) = 1
          AND trk.ship_date > GETDATE() - 180
    UNION
    SELECT trk.tracking,
           trk.ship_via,
           trk.ship_date,
           trk.ship_method,
           trk.doc AS doc__,
           'NJ' AS loc,
           'ONSITE' AS type,
           trk.kid,
           '''order_id'
    FROM track trk WITH (NOLOCK)
    WHERE trk.ledger = 'ONS'
          AND trk.Doc_Type = 'I'
          AND trk.tracking = @scannedItem
          AND trk.ship_date > GETDATE() - 180
    UNION
    SELECT trk.tracking,
           trk.ship_via,
           GETDATE() AS ship_date,
           trk.ship_method,
           CAST(trk.hedkid AS VARCHAR(8)) AS doc__,
           'NJ' AS loc,
           'EMB RETURN' AS type,
           trk.kid,
           '''order_id'
    FROM fe_emb_return_track trk (NOLOCK)
    WHERE trk.tracking = @scannedItem
    UNION
    SELECT trk.tracking,
           trk.ship_via,
           trk.ship_date,
           trk.ship_method,
           hed.doc__,
           hed.loc,
           'PO RETURN' AS type,
           trk.kid,
           '''order_id'
    FROM potrack trk WITH (NOLOCK)
        JOIN poHED hed WITH (NOLOCK)
            ON trk.order_key = hed.order_key
    WHERE trk.tracking = @scannedItem
          AND hed.Doc_Type = 'C'
          AND trk.ship_date > GETDATE() - 180


    DECLARE @itemInfo NVARCHAR(20)

    SET @itemInfo =
    (
        SELECT tracking FROM @trkInfo
    )


    IF (@itemInfo IS NULL)
    BEGIN
        IF @language = 'ES'
        BEGIN
            SET @MessageText = 'Seguimiento: ' + @scannedItem + ' �extraviado! '
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1             -- state
                     );
        END
        ELSE
        BEGIN
            SET @MessageText = 'Tracking: ' + @scannedItem + ' not found! '
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1             -- state
                     );
        END
    END

    IF (@itemInfo IS NOT NULL)
    BEGIN

        DECLARE @orderInvoice INT

        SET @orderInvoice =
        (
            SELECT hed.Order_Complete_
            FROM oetrack trk WITH (NOLOCK)
                JOIN OE61BHED hed WITH (NOLOCK)
                    ON trk.order_key = hed.order_key
            WHERE trk.tracking = @scannedItem
                  AND hed.Doc_Type = 'O'
                  AND trk.ship_date > GETDATE() - 180
        )

        IF (@orderInvoice = 0)
        BEGIN
            IF @language = 'ES'
            BEGIN
                SET @MessageText = 'Ordenar ' + @scannedItem + ' no facturada!'
                RAISERROR(   @MessageText, -- Message text
                             16,           -- severity
                             1             -- state   
                         );
            END
            ELSE
            BEGIN
                SET @MessageText = 'Order ' + @scannedItem + ' not Invoiced!'
                RAISERROR(   @MessageText, -- Message text
                             16,           -- severity
                             1             -- state   
                         );
            END
        END
    ---- ELSE
    ---- BEGIN
    ---- SET @MessageText = 'Order '+ @scannedItem + ' canceled!'
    ---- RAISERROR(
    ----    @MessageText, -- Message text
    ----    16, -- severity
    ----    1 -- state

    ----);
    ----	END



    END --main END

    --check if order blocked
    DECLARE @blockedOrderId NVARCHAR(10)
    DECLARE @blockedType NVARCHAR(10)
    DECLARE @canceledOrder NVARCHAR(20)
    DECLARE @userBlocked NVARCHAR(20)


    SELECT @blockedType = type,
           @blockedOrderId = order_id
    FROM @trkInfo

    SELECT @blockedType = type,
           @blockedOrderId = order_id
    FROM @trkInfo

    SELECT @canceledOrder = order_id,
           @userBlocked = Login
    FROM fe_eod_cancel (nolock)
    WHERE Doc_Type = 'I'
          AND order_id = @blockedOrderId
          AND msg IS NULL

    IF (@blockedType = 'INVOICE' AND @canceledOrder IS NOT NULL)
    BEGIN

        DECLARE @fullsubject NVARCHAR(200)
        DECLARE @fullbody NVARCHAR(500)
        SET @fullsubject = 'Blocked invoice! ' + @scannedItem
        SET @fullbody
            = 'Tracking: ' + @scannedItem + ' Blocked invoice! ' + 'Login: ' + @userBlocked + '. ' + 'Company name: '
              + @shipMethodRaw

        EXEC msdb..sp_send_dbmail @profile_name = 'mail',
                                  @recipients = 'RoyR@FETrading.com;AlanO@FETrading.com',
                                  @subject = @fullsubject,
                                  @body = @fullbody

        SET @MessageText = 'Blocked invoice! ' + @scannedItem
        RAISERROR(   @MessageText, -- Message text
                     16,           -- severity
                     1
                 ) -- state

    END
    ----end blocked

    --Chek if item was scanned before
    DECLARE @scannedBefore NVARCHAR(20)

    SET @scannedBefore =
    (
        SELECT CASE
                   WHEN DATEDIFF(SECOND, scan_date, GETDATE()) >= 86400 THEN
                       CONVERT(VARCHAR(12), DATEDIFF(SECOND, scan_date, GETDATE()) / 60 / 60 / 24) + ' Days ago'
                   WHEN DATEDIFF(SECOND, scan_date, GETDATE()) >= 3600 THEN
                       CONVERT(VARCHAR(12), DATEDIFF(SECOND, scan_date, GETDATE()) / 60 / 60 % 24) + ' Hours ago'
                   WHEN DATEDIFF(SECOND, scan_date, GETDATE()) >= 60 THEN
                       CONVERT(VARCHAR(2), DATEDIFF(SECOND, scan_date, GETDATE()) / 60 % 60) + ' Minutes ago'
                   WHEN DATEDIFF(SECOND, scan_date, GETDATE()) < 60 THEN
                       CONVERT(VARCHAR(2), DATEDIFF(SECOND, scan_date, GETDATE())) + ' Seconds ago'
               END AS 'lastScan'
        FROM zzz_oetrack_scan
        WHERE tracking = @scannedItem
              AND scan_date > GETDATE() - 180
    )

    IF (@scannedBefore IS NOT NULL)
    BEGIN
        IF @language = 'ES'
        BEGIN
            SET @MessageText = 'Seguimiento ' + @scannedItem + ' ya escaneado! ' + '%s'
            RAISERROR(   @MessageText,  -- Message text
                         16,            -- severity
                         1,             -- state
                         @scannedBefore -- first argument to the message text
                     );
        END
        ELSE
        BEGIN
            SET @MessageText = 'Tracking ' + @scannedItem + ' already scanned! ' + '%s'
            RAISERROR(   @MessageText,  -- Message text
                         16,            -- severity
                         1,             -- state
                         @scannedBefore -- first argument to the message text
                     );
        END

    END

    --check company vs pallet

    DECLARE @companyItem NVARCHAR(10)
    DECLARE @itemShipMethod NVARCHAR(10)

    SELECT @companyItem = ship_via
    FROM @trkInfo

    IF (@companyItem != @company OR @companyItem = 'AMAZON_')
    BEGIN
        IF @language = 'ES'
        BEGIN
            SET @MessageText = '�Compa��a de env�o equivocada! ' + @scannedItem
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1
                     ) -- state
        END
        ELSE
        BEGIN
            SET @MessageText = 'Wrong Shipping Company! ' + @scannedItem
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1
                     ) -- state
        END

    END

    DECLARE @palletIdMethod NVARCHAR(20)

    SELECT @palletIdMethod = Method
    FROM zzz_oetrack_scan_pallets
    WHERE pallet = @palletId

    IF @shipMethodRaw = 'Fedex Ground'
       AND @palletIdMethod != 'GROUND'
    BEGIN
        IF @language = 'ES'
        BEGIN
            SET @MessageText = '�M�todo incorrecto! ' + @scannedItem
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1
                     ) -- state
        END
        ELSE
        BEGIN
            SET @MessageText = 'Wrong Method! ' + @scannedItem
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1
                     ) -- state
        END
    END

    IF @shipMethodRaw = 'Fedex Express'
       AND @palletIdMethod != 'EXPRESS'
    BEGIN
        IF @language = 'ES'
        BEGIN
            SET @MessageText = '�M�todo incorrecto! ' + @scannedItem
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1
                     ) -- state
        END
        ELSE
        BEGIN
            SET @MessageText = 'Wrong Method! ' + @scannedItem
            RAISERROR(   @MessageText, -- Message text
                         16,           -- severity
                         1
                     ) -- state
        END
    END

    DECLARE @type NVARCHAR(20),
            @method NVARCHAR(20),
            @shipvia NVARCHAR(20),
            @kitId INT
    SELECT @type = type,
           @method = @shipMethod,
           @shipvia = ship_via,
           @kitId = kid
    FROM @trkInfo
    WHERE tracking = @scannedItem

    DECLARE @outID TABLE (id INT)

    INSERT INTO [dbo].[zzz_oetrack_scan]
    (
        [trkid],
        [tracking],
        [ship_via],
        [Carrier],
        [computer],
        [Login],
        [scan_date],
        [loc],
        [type],
        [ship_method],
        [pallet]
    )
    OUTPUT INSERTED.kid
    INTO @outID
    (
        id
    )
    VALUES
    (@kitId, @scannedItem, @shipvia, @company, @computerName, @badgeId, GETDATE(), @loc, @type, @method, @palletId)

    SELECT kid AS Id,
           pallet AS PalletId,
           tracking AS TrackingId,
           FORMAT(scan_date, 'MM/dd/yyyy hh:mm:s tt') AS Date
    FROM zzz_oetrack_scan WITH (NOLOCK)
    WHERE kid =
    (
        SELECT Id FROM @outID
    )
    ORDER BY kid DESC

END TRY
BEGIN CATCH

    SELECT @ErrorMessage = ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE();

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);

END CATCH
GO