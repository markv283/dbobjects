IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = object_id(N'[dbo].[iwd_eod_GetPalletById]')
          and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_GetPalletById]
END
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[iwd_eod_GetPalletById]
    -- Add the parameters for the stored procedure here
    @palletId nvarchar(10)
AS
DECLARE @ErrorMessage nvarchar(4000),
        @ErrorSeverity int,
        @ErrorState int;
SET NOCOUNT ON;
BEGIN TRY

    SELECT osp.pallet PalletId,
           osp.day_no DayNumber,
           FORMAT(osp.date_opened, 'MM/dd/yyyy hh:mm:s tt') as Date,
           osp.login_opened LoginName,
           COUNT(ost.kid) Count,
           osp.print_label_login as LabelId,
           osp.company + osp.method as ShipMethod
    FROM zzz_oetrack_scan_pallets osp WITH (NOLOCK)
        LEFT JOIN zzz_oetrack_scan ost WITH (NOLOCK)
            ON ost.Pallet = osp.pallet
    WHERE osp.pallet = @palletId
    group by osp.pallet,
             osp.day_no,
             osp.date_opened,
             osp.login_opened,
             osp.print_label_login,
             osp.company + osp.method
    order by date_opened desc

END TRY
BEGIN CATCH

    SELECT @ErrorMessage = ERROR_MESSAGE(),
           @ErrorSeverity = ERROR_SEVERITY(),
           @ErrorState = ERROR_STATE();

    -- return the error inside the CATCH block
    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);

END CATCH
GO


