IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = object_id(N'[dbo].[iwd_eod_GetPalletListByMethod]')
          and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_GetPalletListByMethod]
END
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[iwd_eod_GetPalletListByMethod]
    @shipMethodRaw nvarchar(30),
    @badgeId nvarchar(50)
AS
BEGIN TRY

    SET NOCOUNT ON;

    DECLARE @company nvarchar(30)
    DECLARE @shipMethod nvarchar(30)
    DECLARE @loc nvarchar(10) = 'NJ'

    SET @loc =
    (
        SELECT lg.loc
        FROM [dbo].[itmlst_login] lg WITH (NOLOCK)
        WHERE lg.name = @badgeId
    )

    IF @shipMethodRaw = 'UPS'
    BEGIN
        SET @company = 'UPS'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'FedEx Ground'
    BEGIN
        SET @company = 'FEDEX'
        SET @shipMethod = 'GROUND'
    END
    ELSE IF @shipMethodRaw = 'USPS'
    BEGIN
        SET @company = 'USPS'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'Wholesale'
    BEGIN
        SET @company = 'Wholesale'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'TSC'
    BEGIN
        SET @company = 'TSC'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'FedEx Express'
    BEGIN
        SET @company = 'FEDEX'
        SET @shipMethod = 'EXPRESS'
    END
    ELSE IF @shipMethodRaw = 'PUROLATOR'
    BEGIN
        SET @company = 'PUROLATOR'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'DHL'
    BEGIN
        SET @company = 'DHL'
        SET @shipMethod = ''
    END

    SELECT osp.pallet PalletId,
           osp.day_no DayNumber,
           FORMAT(osp.date_opened, 'MM/dd/yyyy hh:mm:s tt') as Date,
           osp.login_opened LoginName,
           COUNT(ost.kid) Count,
           osp.print_label_login as LabelId
    FROM zzz_oetrack_scan_pallets osp WITH (NOLOCK)
        LEFT JOIN zzz_oetrack_scan ost WITH (NOLOCK)
            ON ost.Pallet = osp.pallet
    WHERE date_truck IS NULL
          AND Company = @company
          AND Method = @shipMethod
          AND osp.loc = @loc
    group by osp.pallet,
             osp.day_no,
             osp.date_opened,
             osp.login_opened,
             osp.print_label_login
    order by osp.pallet desc

END TRY
BEGIN CATCH

    RAISERROR(N'ERROR in SP - dbo_iwd_eod_GetPalletListByMethod', 16, 1);
    ROLLBACK;
END CATCH
GO


