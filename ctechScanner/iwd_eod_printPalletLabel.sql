IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[iwd_eod_printPalletLabel]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_printPalletLabel] 
END
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[iwd_eod_printPalletLabel]
-- Add the parameters for the stored procedure here
@palletId nvarchar(10),
@deviceId nvarchar(20),
@badgeId nvarchar(20)

AS
  DECLARE @ErrorMessage nvarchar(4000),
          @ErrorSeverity int,
          @ErrorState int;
  SET NOCOUNT ON;
  BEGIN TRY

    UPDATE zzz_oetrack_scan_pallets
    SET print_label = GETDATE(),
        print_label_login = @badgeId,
        Print_label_computer = @deviceId
    WHERE pallet = @palletId

		SELECT osp.pallet PalletId ,
		   osp.day_no DayNumber,
		   FORMAT(osp.date_opened,'MM/dd/yyyy hh:mm:s tt') as Date,
		   osp.login_opened LoginName, 
		   COUNT(ost.kid) Count,
		   osp.print_label_login as LabelId
		FROM zzz_oetrack_scan_pallets osp (NOLOCK)
		  LEFT JOIN zzz_oetrack_scan ost (NOLOCK)
			ON ost.Pallet = osp.pallet
			WHERE osp.pallet = @palletId
		group by osp.pallet, osp.day_no, osp.date_opened,osp.login_opened, osp.computer_opened, osp.print_label_login
		order by date_opened desc

  END TRY
  BEGIN CATCH

    SELECT
      @ErrorMessage = ERROR_MESSAGE(),
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE();

    -- return the error inside the CATCH block
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

  END CATCH
GO


