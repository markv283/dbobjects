IF EXISTS
(
    SELECT *
    FROM sysobjects
    WHERE id = object_id(N'[dbo].[iwd_eod_NewPallet]')
          and OBJECTPROPERTY(id, N'IsProcedure') = 1
)
BEGIN
    DROP PROCEDURE [dbo].[iwd_eod_NewPallet]
END
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[iwd_eod_NewPallet]
    @shipMethodRaw nvarchar(30),
    @badgeId nvarchar(50),
    @deviceId nvarchar(20)
AS
BEGIN TRY

    SET NOCOUNT ON;
    DECLARE @dateOpened datetime = GETDATE()
    DECLARE @company nvarchar(30)
    DECLARE @shipMethod nvarchar(30)
    DECLARE @dayNo int
    DECLARE @loc nvarchar(10)

    IF @shipMethodRaw = 'UPS'
    BEGIN
        SET @company = 'UPS'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'FedEx Ground'
    BEGIN
        SET @company = 'FEDEX'
        SET @shipMethod = 'GROUND'
    END
    ELSE IF @shipMethodRaw = 'USPS'
    BEGIN
        SET @company = 'USPS'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'Wholesale'
    BEGIN
        SET @company = 'Wholesale'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'TSC'
    BEGIN
        SET @company = 'TSC'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'FedEx Express'
    BEGIN
        SET @company = 'FEDEX'
        SET @shipMethod = 'EXPRESS'
    END
    ELSE IF @shipMethodRaw = 'PUROLATOR'
    BEGIN
        SET @company = 'PUROLATOR'
        SET @shipMethod = ''
    END
    ELSE IF @shipMethodRaw = 'DHL'
    BEGIN
        SET @company = 'DHL'
        SET @shipMethod = ''
    END

    SET @dayNo =
    (
        SELECT ISNULL(MAX(day_no), 0) + 1 AS md
        FROM zzz_oetrack_scan_pallets WITH (NOLOCK)
        WHERE DATEADD(DAY, DATEDIFF(DAY, 0, date_opened), 0) = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)
    )

    SET @loc =
    (
        SELECT lg.loc
        FROM [dbo].[itmlst_login] lg WITH (NOLOCK)
        WHERE lg.name = @badgeId
    )

    DECLARE @outID TABLE (id int)

    INSERT INTO zzz_oetrack_scan_pallets
    (
        [pallet],
        [day_no],
        [date_opened],
        [login_opened],
        [computer_opened],
        [date_truck],
        [login_truck],
        [computer_truck],
        [truck_no],
        [Company],
        [Method],
        [print_label],
        [print_label_login],
        [Print_label_computer],
        [loc],
        [CvisTruck],
        [CvisTruckCode]
    )
    OUTPUT INSERTED.kid
    INTO @outID
    (
        id
    )
    VALUES
    (NULL,
     @dayNo,
     GETDATE(),
     @badgeId,
     @deviceId,
     NULL,
     NULL,
     NULL,
     NULL,
     @company,
     @shipMethod,
     NULL,
     NULL,
     NULL,
     @loc,
     NULL,
     NULL
    )

    UPDATE zzz_oetrack_scan_pallets
    SET pallet =
        (
            SELECT id FROM @outID
        )
    WHERE kid =
    (
        SELECT id FROM @outID
    )

    SELECT osp.pallet as PalletId,
           osp.day_no as DayNumber,
           osp.login_opened as LoginName,
           FORMAT(osp.date_opened, 'MM/dd/yyyy hh:mm:s tt') as Date,
           0 as Count,
           osp.print_label_login as LabelId
    FROM zzz_oetrack_scan_pallets osp WITH (NOLOCK)
    WHERE osp.pallet =
    (
        SELECT id FROM @outID
    )


END TRY
BEGIN CATCH

    RAISERROR(N'ERROR in SP - dbo.iwd_eod_NewPallet', 1, 1);
END CATCH
GO


